import ReactDOM from 'react-dom';
import 'semantic-ui-css/semantic.min.css';
import registerServiceWorker from './registerServiceWorker';

import './index.css';
import { MainRoute } from './routes'

const route = MainRoute()


ReactDOM.render(route, document.getElementById('root'));
registerServiceWorker();
