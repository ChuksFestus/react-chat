import React from 'react';
import { Redirect, Route, BrowserRouter, Switch } from 'react-router-dom';

import App from './App';
import Auth from './Auth/Auth';
import history from './history';

import Home from './components/Home/Home';
import Profile from './components/Profile/Profile';
import Chat from './components/Chat/Chat';
import Callback from './components/Callback/Callback';


const auth = new Auth();

const handleAuthentication = (nextState, replace) => {
  if (/access_token|id_token|error/.test(nextState.location.hash)) {
    auth.handleAuthentication();
  }
}

export const MainRoute = () => (
  <BrowserRouter history={history} component={App}>'
    <Switch>
      <Route path="/" render={(props) => <App auth={auth} {...props} />} />
      <Route path="/home" render={(props) => <Home auth={auth} {...props} />} />
      <Route path="/chat" render={(props) => (
        !auth.isAuthenticated() ? (
          <Redirect to="/home" />
        ) : (
            <Chat auth={auth} {...props} />
          )
      )} />
      <Route path="/profile" render={(props) => (
        !auth.isAuthenticated() ? (
          <Redirect to="/home" />
        ) : (
            <Profile auth={auth} {...props} />
          )
      )} />
      <Route path="/callback" render={(props) => {
        handleAuthentication(props);
        return <Callback {...props} />
      }} />
    </Switch>
  </BrowserRouter>
)