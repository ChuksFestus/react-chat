import React, { Component } from 'react';
import { Container, Button, Menu} from 'semantic-ui-react'

import './App.css';

class App extends Component {

  goTo(route) {
    this.props.history.replace(`/${route}`)
  }

  login() {
    this.props.auth.login();
  }

  logout() {
    this.props.auth.logout();
  }

  render() {

    const { isAuthenticated } = this.props.auth;

    return (
      <React.Fragment>
        <Menu fixed='top' inverted>
          <Container>
            <Menu.Item as='a' href="/home" header>
              ACCI Chat
            </Menu.Item>
            <Menu.Item>
              <Button onClick={this.goTo.bind(this, 'home')}>Home</Button>
            </Menu.Item>
            <Menu.Item>
              {
                !isAuthenticated() && (
                  <Button onClick={this.login.bind(this)}>Login</Button>
                )
              }
            </Menu.Item>
            <Menu.Item>
              {
                isAuthenticated() && (
                  <Button onClick={this.login.bind(this, 'profile')}>Profile</Button>
                )
              }
            </Menu.Item>
            <Menu.Item>
              {
                isAuthenticated() && (
                  <Button onClick={this.login.bind(this, "chat" )}>Chat</Button>
                )
              }
            </Menu.Item>
            <Menu.Item>
              {
                isAuthenticated() && (
                  <Button onClick={this.logout.bind(this)}>Logout</Button>
                )
              }
            </Menu.Item>
          </Container>
        </Menu>
      </React.Fragment>
    );
  }
}

export default App;
